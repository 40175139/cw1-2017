﻿#region Summary
/* Silvia Balogova
 * handles the certificate to confirm the attendance and presenting of the attendee
 * 25/10/2016
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CourseworkTest
{
    /// <summary>
    /// Interaction logic for Certificate.xaml
    /// </summary>
    public partial class Certificate : Window
    {
        // The certificate to show the attendee attended the conference
        public Certificate(string _firstName, string _secondName, string _conferenceName)
        {
            InitializeComponent();
            txtbCertificate.IsReadOnly = true;
            StringBuilder builder = new StringBuilder();

            builder.AppendLine("This is to certify that " + _firstName + " " + _secondName + " attended " + _conferenceName + ".");

            for(int i = 0; i < builder.Length; i++)
            {
                txtbCertificate.Text += builder[i];
            }
        }

        // The certificate to show the attendee attended and presented at the conference
        public Certificate(string _firstName, string _secondName, string _conferenceName, string _paperTitle)
        {
            InitializeComponent();
            txtbCertificate.IsReadOnly = true;
            StringBuilder builder = new StringBuilder();

            builder.AppendLine("This is to certify thet " + _firstName + " " + _secondName + " attended " + _conferenceName + " and presented a paper entitled " + _paperTitle + ".");

            for(int i = 0; i < builder.Length; i++)
            {
                txtbCertificate.Text += builder[i];
            }
        }

        // Simple OK button to close the window
        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
