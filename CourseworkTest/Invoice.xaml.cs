﻿#region Summary
/* Silvia Balogova
 * handles price display for every attendee
 * 25/10/2016
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CourseworkTest
{
    /// <summary>
    /// Interaction logic for Invoice.xaml
    /// </summary>
    public partial class Invoice : Window
    {
        // The invoice displays how much money attendee should pay
        public Invoice(string _firstName, string _secondName, string _institution, string _conferenceName, int _paid)
        {
            InitializeComponent();
            txtbInvoice.IsReadOnly = true;
            StringBuilder builder = new StringBuilder();

            builder.AppendLine("Name: " + _firstName + " " + _secondName);
            builder.AppendLine("Institution: " + _institution);
            builder.AppendLine("Conference: " + _conferenceName);
            builder.AppendLine("Paid: " + _paid);

            for(int i = 0; i < builder.Length; i++)
            {
                txtbInvoice.Text += builder[i];
            }
        }

        // Simple OK button to close the window
        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
