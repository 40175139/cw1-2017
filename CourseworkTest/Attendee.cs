﻿#region Summary
/* Silvia Balogova
 * handles informations about the attendee
 * 25/10/2016
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseworkTest
{
    class Attendee : Person
    {
        public Institution _institution;

        // Reference number of the attendee
        private int _attendeeRef;
        // Constructor
        public int AttendeeRef
        {
            get { return _attendeeRef; }
            set
            {
                if (value >= 40000 && value <= 60000)
                {
                    _attendeeRef = value;
                }
                else
                {
                    throw new Exception("Input number between 40000 and 60000.");
                }
            }
        }

        // Name of the conference
        private string _conferenceName;
        // Constructor
        public string ConferenceName
        {
            get { return _conferenceName; }
            set
            {
                if (value != "")
                {
                    _conferenceName = value;
                }
                else
                {
                    throw new Exception("Input name of the conference.");
                }
            }
        }
        
        // Type of attendee - full OR student OR organiser
        private string _registrationType;
        // Constructor
        public string RegistrationType
        {
            get { return _registrationType; }
            set
            {
                if (value == "full" || value == "student" || value == "organiser")
                {
                    _registrationType = value;
                }
                else
                {
                    throw new Exception("Input full, student or organiser");
                }
            }
        }

        // Paid or not paid for the conference 
        private bool _paid;
        // Constructor
        public bool Paid
        {
            get { return _paid; }
            set { _paid = value; }
        }

        private bool _presenter;
        public bool Presenter
        {
            get { return _presenter; }
            set { _presenter = value; }
        }

        // Title of the Paper to present
        private string _paperTitle;
        public string PaperTitle
        {
            get { return _paperTitle; }
            set
            {
                if (_presenter == true)
                {
                    if (value != "")
                    {
                        _paperTitle = value;
                    }
                }
                else
                {
                    throw new Exception("Input paper title.");
                }
            }
        }

        public Attendee()
        {
            _institution = new Institution();
        }

        // Method to calculate price for induvidual attendee
        public int GetCost()
        {
            if (_registrationType == "full")
            {
                if (_presenter == true)
                {
                    return 450;
                }
                return 500;
            }
            else if (_registrationType == "student")
            {
                if (_presenter == true)
                {
                    return 270;
                }
                return 300;
            }
            else
            {
                return 0;
            }
        }
    }
}