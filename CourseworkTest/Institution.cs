﻿#region Summary
/* Silvia Balogova
 * handles name and address of institution associated with attendee
 * 25/10/2016
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseworkTest
{
    class Institution 
    {
        // Name of the institution attendee is associated with
        private string _institutionName;
        // Constructor
        public string InstitutionName
        {
            get { return _institutionName; }
            set { _institutionName = value; }
        }

        // Name of the institution attendee is associated with
        private string _institutionAddress;
        public string InstitutionAddress
        {
            get { return _institutionAddress; }
            set { _institutionAddress = value; }
        }
    }
}
