﻿#region Summary
/* Silvia Balogova
 * handles first and second name of attendess
 * 25/10/2016
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseworkTest
{
    abstract class Person
    {
        // First name of the attendee
        private string _firstName;
        // Constructor
        public string FirstName
        {
            get { return _firstName; }
            set
            {
                if (value != "")
                {
                    _firstName = value;
                }
                else
                {
                    throw new Exception("Input name.");
                }
            }
        }

        // Surname - second name of the attendee
        private string _secondName;
        // Constructor
        public string SecondName
        {
            get { return _secondName; }
            set
            {
                if (value != "")
                {
                    _secondName = value;
                }
                else
                {
                    throw new Exception("Input second name.");
                }
            }
        }
    }
}
