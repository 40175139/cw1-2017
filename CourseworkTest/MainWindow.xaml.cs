﻿#region Summary
/* Silvia Balogova
 * main class handles buttons for main GUI
 * 25/10/2016
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CourseworkTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Attendee Attendee;

        public MainWindow()
        {
            InitializeComponent();

            Attendee = new Attendee();
        }
         // Button Set saves all input from the form
        private void btnSet_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Attendee.FirstName = tbxName1.Text;
                Attendee.SecondName = tbxName2.Text;
                Attendee.AttendeeRef = int.Parse(tbxAttendeeRef.Text);
                Attendee._institution.InstitutionName = tbxInstitutionName.Text;
                Attendee._institution.InstitutionAddress = tbxIAddress.Text;
                Attendee.ConferenceName = tbxConferenceName.Text;
                Attendee.RegistrationType = tbxRegistrationType.Text;
                if (chbPaid.IsChecked == true)
                {
                    Attendee.Paid = true;
                }

                if (chbPresenter.IsChecked == true)
                {
                    Attendee.Presenter = true;
                    Attendee.PaperTitle = tbxPaperTitle.Text;
                }
            }
            catch (Exception excep)
            {
                MessageBox.Show(excep.Message, "Error",MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }


        // Button Clear delete all input from the form
        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            tbxName1.Clear();
            tbxName2.Clear();
            tbxAttendeeRef.Clear();
            tbxInstitutionName.Clear();
            tbxIAddress.Clear();
            tbxConferenceName.Clear();
            tbxRegistrationType.Clear();
            chbPaid.IsChecked = false;
            chbPresenter.IsChecked = false;
            tbxPaperTitle.Clear();
        }

        // Button Get will bring all saved content from button Set back to the form
        private void brnGet_Click(object sender, RoutedEventArgs e)
        {
            tbxName1.Text = Attendee.FirstName;
            tbxName1.Text = Attendee.SecondName;
            tbxAttendeeRef.Text = Attendee.AttendeeRef.ToString();
            tbxInstitutionName.Text = Attendee._institution.InstitutionName;
            tbxIAddress.Text = Attendee._institution.InstitutionAddress;
            tbxConferenceName.Text = Attendee.ConferenceName;
            tbxRegistrationType.Text = Attendee.RegistrationType;
            
            if (Attendee.Paid == true)
            {
                chbPaid.IsChecked = true;
            }
            else
            {
                chbPaid.IsChecked = false;
            }

            if (Attendee.Presenter == true)
            {
                chbPresenter.IsChecked = true;
            }
            else
            {
                chbPresenter.IsChecked = false;
            }

            tbxPaperTitle.Text = Attendee.PaperTitle;
        }

        // Button Invoice displays an invoice for attendee - how much they should pay
        private void btnInvoice_Click(object sender, RoutedEventArgs e)
        {
            Invoice invoice = new Invoice(Attendee.FirstName, Attendee.SecondName, Attendee._institution.InstitutionName,
                Attendee.ConferenceName, Attendee.GetCost());

            invoice.ShowDialog();
        }

        // Button Certificate creates a certificate to confirm attendance of the attendee
        private void btnCertificate_Click(object sender, RoutedEventArgs e)
        {
            if(Attendee.Presenter == true)
            {
                Certificate certificate = new Certificate(Attendee.FirstName, Attendee.SecondName,
                    Attendee.ConferenceName, Attendee.PaperTitle);
                certificate.ShowDialog();
            }
            else
            {
                Certificate certificate = new Certificate(Attendee.FirstName, Attendee.SecondName,
                    Attendee.ConferenceName);
                certificate.ShowDialog();
            }
        }
    }
}
